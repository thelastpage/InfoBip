package com.infobip.url.domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import org.springframework.data.annotation.Transient;
import com.infobip.url.dto.UrlDto;

@Entity
@Table(name="url")
public class Url {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	private String longUrl;
	private String shortUrl;
	private String userId;
	private int redirectType;
	private long urlRedirect; //stores count of url redirects
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLongUrl() {
		return longUrl;
	}
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
	
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	public long getUrlRedirect() {
		return urlRedirect;
	}
	public void setUrlRedirect(long urlRedirect) {
		this.urlRedirect = urlRedirect;
	}
	public int getRedirectType() {
		return redirectType;
	}
	public void setRedirectType(int redirectType) {
		this.redirectType = redirectType;
	}
	
	@Transient
	public UrlDto urlDto() {		
		UrlDto urlDto =new UrlDto();
		urlDto.setLongUrl(longUrl);	
		urlDto.setRedirectType(redirectType);
		
		return urlDto;
	}
}
