package com.infobip.url.controller;

import java.net.URI;
import java.net.URISyntaxException;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.infobip.url.dto.LongUrlDto;
import com.infobip.url.dto.ShortUrlDto;
import com.infobip.url.dto.StatisticDto;
import com.infobip.url.dto.UrlDto;
import com.infobip.url.exception.ServiceException;
import com.infobip.url.service.AccountService;
import com.infobip.url.service.UrlService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value="Short Url Creation", description="Operations pertaining to Urls") 
public class UrlController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	UrlService urlService;
	
	@PostMapping("/register")
	@ApiOperation(value = "Get short url", response = Iterable.class)
	public ResponseEntity<ShortUrlDto> registerUrl(HttpServletRequest request,@RequestBody LongUrlDto longUrlDto) throws ServiceException{
		String authhead = request.getHeader("Authorization");
		String accountId=accountService.authenticateuser(authhead);
		UrlDto urlDto=new UrlDto();
		urlDto.setLongUrl(longUrlDto.getLongUrl());
		urlDto.setUserId(accountId);
		ShortUrlDto url= urlService.generateShortUrl(urlDto);
		return new ResponseEntity<ShortUrlDto>(url, HttpStatus.OK);
	}
	
	
	@GetMapping("/statistic/{AccountId}")
	@ApiOperation(value = "Get Account Statistics", response = Iterable.class)
	public ResponseEntity<StatisticDto> getStatistics(HttpServletRequest request,@PathVariable("AccountId") String accId) throws ServiceException{
		
		String authhead = request.getHeader("Authorization");				
		accountService.authenticateuser(authhead);
		StatisticDto stats= urlService.getStatistics(accId);			
		return new ResponseEntity<StatisticDto>(stats, HttpStatus.OK);		
	}
	
	@GetMapping("/st/{shortUrl}")
	@ApiOperation(value = "Redirect using short url", response = Iterable.class)
	public ResponseEntity<String> redirectUrl(@PathVariable("shortUrl") String shortUrl) throws ServiceException{
		
		HttpHeaders header=new HttpHeaders();
		UrlDto url = urlService.redirect(shortUrl);	
		try {
			header.setLocation(new URI(url.getLongUrl()));
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			throw new ServiceException("URI malperformed!");
		}
		return new ResponseEntity<String>("Redirect Success",header, url.getRedirectType()==301?HttpStatus.PERMANENT_REDIRECT:HttpStatus.TEMPORARY_REDIRECT);		
	}
}
