package com.infobip.url.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.infobip.url.dto.AccountIdDto;
import com.infobip.url.dto.UserDto;
import com.infobip.url.exception.ServiceException;
import com.infobip.url.service.AccountService;
import com.infobip.url.validator.UserValidator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@Api(value="Account Creation", description="Operations pertaining to creation of accounts") 
public class AccountController {
	
	@Autowired
	AccountService accountService;
	
	@Autowired
	UserValidator userValidator;
		
	@PostMapping("/account")
	@ApiOperation(value = "Register an account", response = Iterable.class)
	public @ResponseBody ResponseEntity<UserDto> accountRegistration(@RequestBody AccountIdDto accountDto, BindingResult result) throws ServiceException{
		
		userValidator.validate(accountDto, result);
		
		if(result.hasErrors()) {
			throw new ServiceException(result.getFieldError().getDefaultMessage());
		}
		UserDto user=new UserDto();
		user.setAccountId(accountDto.getAccountId());
		UserDto userDetails=accountService.registerUser(user);
		
		if(userDetails.isSuccess())
			return new ResponseEntity<UserDto>(userDetails, HttpStatus.OK);
		else
			return new ResponseEntity<UserDto>(userDetails, HttpStatus.IM_USED);
	}

}
