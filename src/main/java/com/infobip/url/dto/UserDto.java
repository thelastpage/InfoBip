package com.infobip.url.dto;

import java.io.Serializable;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.infobip.url.domain.User;

public class UserDto implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String accountId;
	private String password;
	private String hashPassword;
	
	private boolean success=false;
	private String description;

	public boolean isSuccess() {
		return success;
	}
	public void setSuccess(boolean success) {
		this.success = success;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getAccountId() {
		return accountId;
	}
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}	

	@JsonIgnore
	@JsonProperty(value="hashPassword")
	public String getHashPassword() {
		return hashPassword;
	}
	public void setHashPassword(String hashPassword) {
		this.hashPassword = hashPassword;
	}
	
	@JsonIgnore
	@JsonProperty(value="user")
	public User getUser() {
		User user=new User();
		user.setPassword(hashPassword);
		user.setUserId(accountId);
		return user;
	}
}
