package com.infobip.url.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.infobip.url.domain.Url;

public class UrlDto {
	
	private String longUrl;
	private String shortUrl;
	private int redirectType;
	private long countOfRedirects;

	@JsonIgnore
	@JsonProperty(value="userId")
	private String userId;
	
	public String getLongUrl() {
		return longUrl;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public void setLongUrl(String longUrl) {
		this.longUrl = longUrl;
	}
	public String getShortUrl() {
		return shortUrl;
	}
	public void setShortUrl(String shortUrl) {
		this.shortUrl = shortUrl;
	}
	public int getRedirectType() {
		return redirectType;
	}
	public void setRedirectType(int redirectType) {
		this.redirectType = redirectType;
	}
	public long getCountOfRedirects() {
		return countOfRedirects;
	}
	public void setCountOfRedirects(long countOfRedirects) {
		this.countOfRedirects = countOfRedirects;
	}
	
	@JsonIgnore
	@JsonProperty(value="url")
	public Url getUrl() {
		Url url=new Url();
		url.setLongUrl(longUrl);
		url.setShortUrl(shortUrl);
		url.setUserId(userId);
		url.setRedirectType(redirectType);
		return url;
	}
}
