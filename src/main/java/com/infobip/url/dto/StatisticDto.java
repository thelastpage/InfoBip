package com.infobip.url.dto;

import java.util.HashMap;

public class StatisticDto {
	
	private HashMap<String, Long> urlStatistics;

	public HashMap<String, Long> getUrlStatistics() {
		return urlStatistics;
	}

	public void setUrlStatistics(HashMap<String, Long> urlStatistics) {
		this.urlStatistics = urlStatistics;
	}
}
