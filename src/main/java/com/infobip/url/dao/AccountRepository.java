package com.infobip.url.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.infobip.url.domain.User;

@Repository
public interface AccountRepository extends JpaRepository<User, Integer> {
	
	User findByUserId(String userId);
	User findByUserIdAndPassword(String userId,String password);
}
