package com.infobip.url.dao;

import java.util.ArrayList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.infobip.url.domain.Url;

@Repository
public interface UrlRepository extends JpaRepository<Url, Integer> {
	
	Url findFirstByOrderByIdDesc();
	ArrayList<Url> findByUserId(String userId);
	Url findByLongUrl(String url);
	
}
