package com.infobip.url.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import com.infobip.url.dto.AccountIdDto;

@Component
public class UserValidator implements Validator {

	public boolean supports(Class<?> arg0) {
		// TODO Auto-generated method stub
		return AccountIdDto.class.isAssignableFrom(arg0);
	}
	
	@Override
	public void validate(Object obj, Errors error) {
		
		AccountIdDto userDetails=(AccountIdDto)obj;	
		
		String accountId= userDetails.getAccountId();
		if(null==accountId || accountId.length()==0) {
			error.rejectValue("accountId", "invalidValue", new Object[]{"'accountId'"}, "AccountId is required!");
		}
	}


}
