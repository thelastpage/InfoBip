package com.infobip.url.service;

import com.infobip.url.dto.ShortUrlDto;
import com.infobip.url.dto.StatisticDto;
import com.infobip.url.dto.UrlDto;
import com.infobip.url.exception.ServiceException;

public interface UrlService {

	ShortUrlDto generateShortUrl(UrlDto urlDto) throws ServiceException;

	StatisticDto getStatistics(String accId) throws ServiceException;

	UrlDto redirect(String shortUrl) throws ServiceException;

}
