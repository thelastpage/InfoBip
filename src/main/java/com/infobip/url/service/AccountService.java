package com.infobip.url.service;

import javax.validation.Valid;

import com.infobip.url.dto.UserDto;
import com.infobip.url.exception.ServiceException;

public interface AccountService {

	UserDto registerUser(@Valid UserDto user) throws ServiceException;

	String authenticateuser(String authhead) throws ServiceException;
	
}
