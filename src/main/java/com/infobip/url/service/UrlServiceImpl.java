package com.infobip.url.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.infobip.url.dao.UrlRepository;
import com.infobip.url.domain.Url;
import com.infobip.url.dto.ShortUrlDto;
import com.infobip.url.dto.StatisticDto;
import com.infobip.url.dto.UrlDto;
import com.infobip.url.exception.ServiceException;

@Service
public class UrlServiceImpl implements UrlService{
	
	@Autowired
	UrlRepository urlRepository;
	
	@Value("${server.port}")
	int port;
	
	@Override
	public ShortUrlDto generateShortUrl(UrlDto urlDto) throws ServiceException {
		
		String url= urlDto.getLongUrl();
		
		//check if shortUrl already exists
		Url longUrlDto=urlRepository.findByLongUrl(url);
		if(null!= longUrlDto) {
			ShortUrlDto sDto=new ShortUrlDto();
			sDto.setShortUrl("http://localhost:"+port+"/"+longUrlDto.getShortUrl());
			return sDto;
		}
		
		//if short url doesn't exists , create the short url 
	
		String shortenedUrl= encodeLongUrl(getIndex());
		urlDto.setShortUrl(shortenedUrl);
		urlRepository.saveAndFlush(urlDto.getUrl());
		ShortUrlDto sDto=new ShortUrlDto();
		sDto.setShortUrl("http://short.com/"+shortenedUrl);
		return sDto;
	}


	private int getIndex() {
		Url record= urlRepository.findFirstByOrderByIdDesc();
		if(null==record)
			return 1;
		else return record.getId()+1;
	}


	private String encodeLongUrl( int id) {
		
		ArrayList<Integer> list=getBase62(id);
		String encodedUrl= generateUrl(list);
		return encodedUrl;
	}

	private String generateUrl(ArrayList<Integer> list) {
		
		StringBuilder str= new StringBuilder();		
		for(Integer i: list) {
			if(i<26)
				str.append((char)(i+65));
			else if(i<52)
				str.append((char)(i+71));
			else
				str.append((char)(i-4));			
		}		
		return str.toString();
	}

	private ArrayList<Integer> getBase62(int id) {		
		ArrayList<Integer> encodeId=new ArrayList<>();		
		while(id>0) {
			int rem= id%62;
			encodeId.add(rem);
			id=id/62;
		}		
		Collections.reverse(encodeId);
		return encodeId;	}

	@Override
	public StatisticDto getStatistics(String accId) throws ServiceException {
		
		ArrayList<Url> list= urlRepository.findByUserId(accId);
		HashMap<String, Long> map=new HashMap<>();
		for(Url obj:list)
			map.put(obj.getLongUrl(), obj.getUrlRedirect());
		StatisticDto stDto=new StatisticDto();
		stDto.setUrlStatistics(map);
		
		return stDto;
	}

	@Override
	public UrlDto redirect(String shortUrl) throws ServiceException {
		// TODO Auto-generated method stub
		String arr[]= shortUrl.split("/");
		StringBuilder shortUri= new StringBuilder(arr[arr.length-1]);
		StringBuilder redirectIndex=new StringBuilder();
		for(int i=0;i<shortUri.length();i++) {
			char ch=shortUri.charAt(i);
			if(Character.isLowerCase(ch))
				redirectIndex.append((int)ch-97);
			else if(Character.isUpperCase(ch))
				redirectIndex.append((int)ch-65);
			else if(Character.isDigit(ch))
				redirectIndex.append((int)ch+4);
			else
				throw new ServiceException("Invalid Url");
		}
		
		int id= Integer.parseInt(redirectIndex.toString());
		int index= getFromBase62(id);
		Url url= urlRepository.getOne(index);
		if(null==url)
			throw new ServiceException("No matching route found");
		
		url.setUrlRedirect(url.getUrlRedirect()+1);
		urlRepository.saveAndFlush(url);
		
		return url.urlDto();
	}
	
	private int getFromBase62(int id) {
		int index=0;
		int k=0;
		while(id>0) {
			int rem=id%10;
			index+=rem* Math.pow(64, k);
			k++;
			id=id/10;
		}
		return index;
	}

}
