package com.infobip.url.service;

import java.util.Base64;

import javax.validation.Valid;
import org.apache.commons.lang3.RandomStringUtils;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.infobip.url.dao.AccountRepository;
import com.infobip.url.domain.User;
import com.infobip.url.dto.UserDto;
import com.infobip.url.exception.ServiceException;

@Service
public class AccountServiceImpl implements AccountService{
	
	@Autowired
	AccountRepository accountRepository;
	
	@Override
	public UserDto registerUser(@Valid UserDto user) throws ServiceException {
		try {
			if(null==accountRepository.findByUserId(user.getAccountId())) {
				user.setPassword(genPassword(user.getAccountId()));
				user.setHashPassword(hashPassword(user.getPassword()));
				accountRepository.saveAndFlush(user.getUser());
				user.setSuccess(true);
				user.setDescription("Your account is opened");
			}
			else {
				user.setSuccess(false);
				user.setDescription("Account with that ID already exists");
			}	
		}
		catch(Exception e) {
			throw new ServiceException("Error Encountered while creating User");
		}
		return user;
	}

	@Override
	public String authenticateuser(String authhead) throws ServiceException {
		
		String user="";		
		if (authhead != null) {			
			byte[] e = Base64.getMimeDecoder().decode(authhead.substring(6));
			String usernpass = new String(e);
			user = usernpass.substring(0, usernpass.indexOf(":"));
			String password = usernpass.substring(usernpass.indexOf(":") + 1);	
			User userObj=accountRepository.findByUserId(user);
			if(null==userObj)
				throw new ServiceException("No such account exists");
			else if(!BCrypt.checkpw(password, userObj.getPassword())) {
				throw new ServiceException("Invalid Credentials");
			}
		}
		else
			throw new ServiceException("Missing Credentials");
		
		return user;
	}

	private String genPassword(String accountId) {
		return RandomStringUtils.randomAlphanumeric(8);
	}

	private String hashPassword(String randomAlphanumeric) {
		// TODO Auto-generated method stub
		return BCrypt.hashpw(randomAlphanumeric, BCrypt.gensalt(10));
	}	
	
}
